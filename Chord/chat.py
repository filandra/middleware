"""
	4. domácí úkol - Adam Filandr
"""

# With this, it is not necessary to adjust python path in order to work with the chord module
import sys
sys.path.insert(0, 'chord')

# This adds backward compatibility for Python2
try: input = raw_input
except NameError: pass


from chord import Local
# from remote import Remote
from address import Address
from settings import SIZE
# from chord import retry_on_socket_error
import json
import socket
# import threading
import hashlib
import time
import threading

channels_lock = threading.RLock()
names_lock = threading.RLock()


my_channels = {}
joined_channels = []
name_to_id = {}

def hash_string(string):
	"""Produces a chord hash (id) for a string"""

	# Hash string to integer (stable across executions, processes, nodes)
	sha256 = hashlib.sha256()
	sha256.update(string.encode('utf-8'))
	digest = sha256.hexdigest()
	ret = int(digest, 16) % SIZE

	# Hash used to be stable, but now it is salted, do not use it here !
	#ret = hash(string) % SIZE

	return ret

class Chat(object):
	"""Data structure that represents a member of a chat network."""

	def __init__(self, local_address, remote_address, name):
		"""
		:param local_address: address of the local node.
		:param remote_address: address of the remote node to bootstrap from.
		:param name: name belonging to the local node (name of the local user)
		"""

		self.local_ = Local(local_address, remote_address)  # local chord node

		self.local_.register_command("set", self._accept_msg)
		self.local_.register_command("join", self._accept_join)
		self.local_.register_command("leave", self._accept_leave)
		self.local_.register_command("send", self._accept_send)
		self.local_.register_command("transfer", self._accept_transfer)
		self.local_.register_command("get", self._accept_get_name)
		self.local_.register_command("setter", self._accept_set_name)
		self.local_.register_command("return", self._accept_name_return)
		self.local_.register_command("name", self._accept_name)
		self.local_.set_notify_handler(self._predecessor_change)

		self.local_.start()  # starting the local chord node
		self.msg_handler_ = None  # handler for incoming chat messages
		self.join_handler_ = None  # handler for incoming joins
		self.leave_handler_ = None  # handler for incoming leave requests
		self.send_handler_ = None  # handler for incoming send requests
		self.transfer_handler_ = None  # handler for incoming send requests
		self.set_name_handler_ = None  # handler for setting name
		self.get_name_handler_ = None  # handler for getting name
		self.name_return_handler_ = None  # handler for returning name
		self.name_handler_ = None  # handler for getting name

		self.name = name
		self.local_address = local_address



	def set_msg_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.msg_handler_ = handler

	def _accept_msg(self, msg):
		"""Handler of a msg command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.msg_handler_ is not None:
				self.msg_handler_(msg['value'])
			else:
				print('received message not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_join_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.join_handler_ = handler

	def _accept_join(self, msg):
		"""Handler of a join command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.join_handler_ is not None:
				self.join_handler_(msg['joinee_id'], msg['channel_id'])
			else:
				print('received join not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_leave_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.leave_handler_ = handler

	def _accept_leave(self, msg):
		"""Handler of a leave command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.leave_handler_ is not None:
				self.leave_handler_(msg['leaver_id'], msg['channel_id'])
			else:
				print('received leave not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_send_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.send_handler_ = handler

	def _accept_send(self, msg):
		"""Handler of a send command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.send_handler_ is not None:
				self.send_handler_(msg['msg'], msg['channel_id'])
			else:
				print('received send not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_transfer_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.transfer_handler_ = handler

	def _accept_transfer(self, msg):
		"""Handler of a transfer command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.transfer_handler_ is not None:
				self.transfer_handler_(msg['channel_id'], msg['channel'])
			else:
				print('received transfer not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_set_name_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.set_name_handler_ = handler

	def _accept_set_name(self, msg):
		"""Handler of a set_name command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.set_name_handler_ is not None:
				self.set_name_handler_(msg['name'], msg['setter_id'])
			else:
				print('received set name not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_get_name_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.get_name_handler_ = handler

	def _accept_get_name(self, msg):
		"""Handler of a get_name command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.get_name_handler_ is not None:
				return str(self.get_name_handler_(msg['name'], msg['getter_id']))
			else:
				print('received get name not for us but for: ', msg['key'])
				return str(-1)
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_name_return_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.name_return_handler_ = handler

	def _accept_name_return(self, msg):
		"""Handler of a get_name command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.name_return_handler_ is not None:
				return str(self.name_return_handler_(msg['name'], msg['node_id']))
			else:
				print('received name return not for us but for: ', msg['key'])
				return str(-1)
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''

	def set_name_handler(self, handler):
		"""Sets handler of the incoming messages. The handler must be
		a function accepting a message as its first parameter."""
		self.name_handler_ = handler

	def _accept_name(self, msg):
		"""Handler of name command."""
		try:
			msg = json.loads(msg)
			if int(msg['key']) == self.local_.id() \
					and self.name_handler_ is not None:
				self.name_handler_(msg['name'], msg['name_owner'])
			else:
				print('received name not for us but for: ', msg['key'])
		except (KeyError, ValueError) as error:
			print("bad message accepted", error)
		return ''




	def send_msg_id(self, node_id, msg, channel_id):
		"""Attempts to send a message msg to a node with id node_id."""
		try:
			node_id = node_id % SIZE
			str_hash = str(node_id)
			print("trying to send message to: " + str(node_id))
			node = self.local_.find_successor(node_id)
			node.user_command("set", json.dumps({
				'key': str_hash,
				'value': 'message from channel ' + str(channel_id) + ": " + msg
			}))
			print("message sent")
		except socket.error:
			print("send failed")

	def send_msg(self, node_name, msg):
		"""Attempts to send a message msg to a node with id node_id."""
		try:
			# Oprava čekacího while cyklu místo returnu
			node_with_name = int(self.get_name(node_name))

			# Oprava chyby, která vznikala při posílání msg neexistujícímu jménu
			if node_with_name == -1:
				print("Message send aborted - could not find node with this name.")
				return

			print("trying to send message to: " + node_name \
				  + " with hash: " + str(node_with_name))
			node = self.local_.find_successor(node_with_name)
			node.user_command("set", json.dumps({
				'key': str(node_with_name),
				'value': 'message from node ' + self.name + ": " + msg
			}))
			print("message sent")
		except socket.error:
			print("send failed")

	def join_request(self, joinee_id, channel_name):
		try:
			channel_id = hash_string(channel_name)
			print("Joining " + channel_name + " with id: " + str(channel_id))
			node = self.find_channel_adress(channel_id)
			node_id = node.id()
			str_hash = str(node_id)
			print("Node responsible has id: " + str(node_id))
			node.user_command("join", json.dumps({
				'key': str_hash,
				'joinee_id': joinee_id,
				'channel_id': str(channel_id)
			}))
			joined_channels.append(channel_name)
		except socket.error:
			print("Join request failed")

	def leave_request(self, leaver_id, channel_name):
		try:
			if channel_name in joined_channels:
				channel_id = hash_string(channel_name)
				print("Leaving " + channel_name + " with id: " + str(channel_id))
				node = self.find_channel_adress(channel_id)
				node_id = node.id()
				str_hash = str(node_id)
				print("Node responsible has id: " + str(node_id))
				node.user_command("leave", json.dumps({
					'key': str_hash,
					'leaver_id': leaver_id,
					'channel_id': str(channel_id)
				}))
				# Oprava chyby, kdy se neodebírali channely z joined_channels
				joined_channels.remove(channel_name)
			else:
				print("We are not joined to this channel.")
		except socket.error:
			print("Leave request failed")

	def send_request(self, msg, channel_name):
		try:
			channel_id = hash_string(channel_name)
			print("Sending to " + channel_name + " with id: " + str(channel_id))
			node = self.find_channel_adress(channel_id)
			node_id = node.id()
			str_hash = int(node_id)
			print("Node responsible has id: " + str(node_id))
			node.user_command("send", json.dumps({
				'key': str_hash,
				'msg': msg,
				'channel_id': int(channel_id)
			}))
		except socket.error:
			print("Send request failed")

	def transfer_channel(self, node, channel):
		try:
			node_id = node.id()
			str_hash = str(node_id)
			print("Transfering channel " + channel + " to: " + str(node_id))
			#node = self.local_.find_successor(node_id)
			node.user_command("transfer", json.dumps({
				'key': str_hash,
				'channel_id': channel,
				'channel': my_channels[channel]
			}))
		except socket.error:
			print("Transfer failed")

	def set_name(self):
		try:
			name_id = hash_string(self.name)
			print("Setting name " + self.name + " with id: " + str(name_id))
			node = self.find_channel_adress(name_id)
			node_id = node.id()
			str_hash = str(node_id)
			print("Node responsible has id: " + str(node_id))
			node.user_command("setter", json.dumps({
				'key': str_hash,
				'name': self.name,
				'setter_id': (hash(self.local_address))
			}))
		except socket.error:
			print("Set request failed")

	def get_name(self, name):
		try:
			name_id = hash_string(name)
			print("Getting name " + name + " with id: " + str(name_id))
			node = self.find_channel_adress(name_id)
			node_id = node.id()
			str_hash = str(node_id)
			print("Node holding name map has id: " + str(node_id))
			return node.user_command("get", json.dumps({
				'key': str_hash,
				'name': name,
				'getter_id': (hash(self.local_address))
			}))
		except socket.error:
			print("Get request failed")

	def name_return(self, name, return_id):
		try:
			node = self.find_channel_adress(return_id)
			# Oprava chyby, která vznikala při posílání msg neexistujícímu jménu
			if name in name_to_id:
				node_id = name_to_id[name]
				print("Returning name " + name + " which belongs to id: " + str(node_id))
				return node.user_command("return", json.dumps({
					'key': str(return_id),
					'name': name,
					'node_id': node_id
				}))
			else:
				print("Node with name " + name + " does not exist")
				return node.user_command("return", json.dumps({
					'key': str(return_id),
					'name': name,
					'node_id': -1
				}))
		except socket.error:
			print("Return request failed")

	def transfer_name(self, node, name, name_owner):
		try:
			node_id = node.id()
			str_hash = str(node_id)
			if(name_owner==""):
				print("Deleting name: " + name \
					  + " from node with hash: " + str_hash)
			else:
				print("Transfering name: " + name \
				  + " to node with hash: " + str_hash)
			#node = self.local_.find_successor(node_id)
			node.user_command("name", json.dumps({
				'key': str_hash,
				'name': name,
				'name_owner': name_owner
			}))
		except socket.error:
			print("Transfer failed")

	def _predecessor_change(self, new_predecessor):
			to_delete = []
			to_delete_names = []
			# Kvůli predecessor change při nastartování prvního node
			if(self.local_address != new_predecessor.id()):
				print("New predecessor " + str(new_predecessor.id()) + " joined")
				with channels_lock:
					for channel in my_channels:
						if(not self.local_.is_ours(int(channel))):
							print("Channel " + channel + " no longer belongs to us.")
							self.transfer_channel(new_predecessor, channel)
							to_delete.append(channel)

					for channel in to_delete:
						del my_channels[channel]

				with names_lock:
					for name in name_to_id:
						if (not self.local_.is_ours(hash_string(name))):
							print("Name " + name + " no longer belongs to us.")
							self.transfer_name(new_predecessor, name, name_to_id[name])
							to_delete_names.append(name)

					for name in to_delete_names:
						del name_to_id[name]


	def find_channel_adress(self, channel_id):
		# TODO možná špatné hledání nodu, ale spíš ne
		return self.local_.find_successor(channel_id)

	def shutdown(self):
		"""Shuts down the chat node."""
		print("shutting down ...")
		#Delete my name
		self.transfer_name(self.local_.find_successor(hash_string(self.name)), self.name, "")

		# Oprava 2 - řeší pripad, kdy se obcas vlastnik sam nevymazal
		# a kdy se po ukonceni klient snazil poslat channel sam sobě
		list = joined_channels.copy()
		for channel_name in list:
			self.leave_request(str(hash(self.local_address)), channel_name)
		successor = self.local_.successor()
		# No need to delete my_channels since we are quitting
		with channels_lock:
			for channel in my_channels:
				self.transfer_channel(successor, channel)
		# No need to delete name_to_id since we are quitting
		with names_lock:
			for name in name_to_id:
				self.transfer_name(successor, name, name_to_id[name])


		self.local_.shutdown()


def print_program_usage():
	"""Prints program usage to a standard output"""
	print("Usage:")
	print("python chat.py [-j <bootstrap_address>] <our_address> <name>")
	print("where <name> is a name under which we want to be known in the chat")
	print("and <our_address> is an address of our local machine " \
		  "where we will be listening (it also determines an id of out node)")
	print("if only <our_address> and <name> are given a new chord " \
		  "network is started with only our node")
	print("if the optional [-j <bootstrap_address>] is given our node " \
		  "will join an existing chord network through")
	print("node on address <bootstrap_address>")
	print()
	print("<*_address> may be a port number, then the IP address " \
		  "will be determined by gethostbyname(gethostname())")
	print("which will be an IPv4 address of some network interface we have")
	print("or it may be in format address:port where address is an " \
		  "IPv4 address or domain name,")
	print("for example: 77.75.72.3:5000 or localhost:6000")


def print_usage():
	"""Prints commands that the program accepts on the standard input."""
	print("Usage:")
	print("msg <name> <message> - sends a message to peer identified by <name>")
	print("send <channel> <message> - sends a message to channel name <channel>")
	print("join <channel> - joins a <channel> - " \
		  "we receive message from the channel afterwards")
	print("leave <channel> - leaves a joined channel")
	print("exit - exits the program")


def is_hundred_and_twenty_seven(ip_address):
	"""Returns whether given ip address (string) is the 127.0.0.1 address."""
	return [int(x) for x in ip_address.split('.')] == [127, 0, 0, 1]


def parse_int(string):
	"""Attempts to convert given parameter to an int, returns the
	integer value or None when the conversion fails."""
	try:
		return int(string)
	except ValueError:
		return None


def parse_address(string):
	"""Parses network address from the 'ip:port' or 'port' format."""
	tmp = string.split(":", 1)
	try:
		hostname = socket.gethostname()
		port = None
		if len(tmp) == 1:  # only port was given address is localhost
			port = parse_int(string)
		elif len(tmp) == 2:  # address:port was given
			hostname = socket.gethostbyname(tmp[0])
			port = parse_int(tmp[1])
		if port is not None:
			return Address(hostname, port)
		raise ValueError('wrong address: ' + string)
	except socket.gaierror as e:
		raise ValueError("wrong address: " + string)


def parse_program_params():
	"""Attempts to load console parameters."""
	local_address = None  # out address
	remote_address = None  # address of the bootstrap node
	name = None  # our chat name

	try:
		if len(sys.argv) == 3:
			local_address = parse_address(sys.argv[1])
			name = sys.argv[2]
		elif len(sys.argv) == 5 and sys.argv[1] == '-j':
			remote_address = parse_address(sys.argv[2])
			local_address = parse_address(sys.argv[3])
			name = sys.argv[4]
		else:
			print_program_usage()
			exit()
	except ValueError as error:
		print(error)
		print()
		print_program_usage()
		exit()

	if remote_address is not None and \
			(is_hundred_and_twenty_seven(remote_address.ip) and
				 not is_hundred_and_twenty_seven(local_address.ip) or
					 not is_hundred_and_twenty_seven(remote_address.ip) and
					 is_hundred_and_twenty_seven(local_address.ip)):
		print("error: either both ip addresses have to be" \
			  " 127.0.0.1 or none of them")
		print_program_usage()
		exit()

	return (local_address, remote_address, name)


def main_program():
	# TODO Consider, co se může pokazit / zlepšit:
	# Při připojování se nemusím připjit k channelu - nezjistil bych to. Dá se opravit posláním confirm zprávy
	# Dvě stejný jména mohou kolidovat - vyřešilo by se to zákazem kolidujících jmen
	# Pokud nedochází ke změně v síti, nejspíš není potřeba pokaždé vyptávat adresu nodu podle jména, ale držet si tabulku
		# Ze zadání jsem pochopil, že se máme pokaždé vyptat


	"""Main function of the chat program."""
	print("Welcome in the Chat!")

	local_address, remote_address, name = parse_program_params()

	def msg_printer(msg):
		"""Handler of an incoming message, prints the message to the
		standard output."""
		print(msg)

	def join_registered(joinee_id, channel_id):
		with channels_lock:
			print("Adding " + joinee_id + " to channel " + channel_id)
			if(channel_id not in my_channels):
				my_channels[channel_id] = []
			if(joinee_id not in my_channels[channel_id]):
				my_channels[channel_id].append(joinee_id)
			print("My channels now are: " + str(my_channels))

	def leave_registered(leaver_id, channel_id):
		with channels_lock:
			print("Removing " + leaver_id + " from channel " + channel_id)
			if(channel_id in my_channels):
				if(leaver_id in my_channels[channel_id]):
					my_channels[channel_id].remove(leaver_id)
					if(my_channels[channel_id] == []):
						del my_channels[channel_id]
			print("My channels now are: " + str(my_channels))

	def send_registered(msg, channel_id):
		with channels_lock:
			for node_id in my_channels[str(channel_id)]:
				chat.send_msg_id(int(node_id), msg, channel_id)

	def transfer_registered(channel_id, transfered_channel):
		with channels_lock:
			print("Accepting channel " + channel_id + " with participants " + str(transfered_channel))
			if (channel_id not in my_channels):
				my_channels[channel_id] = []
			for participant in transfered_channel:
				if (participant not in my_channels[channel_id]):
					my_channels[channel_id].append(participant)
			print("My channels now are: " + str(my_channels))

	def set_name_registered(name, setter_id):
		with names_lock:
			print("Registering " + name + " with id: " + str(hash_string(name)))
			name_to_id[name] = setter_id

	def get_name_registered(name, getter_id):
		print("Looking up " + name + " for node with id: " + str(getter_id))
		return chat.name_return(name, getter_id)

	def name_return_registered(name, node_id):
		if node_id!=-1:
			print("Id for " + name + " returned with value: " + str(node_id))
		return node_id

	def name_registered(name, owner_id):
		with names_lock:
			if (owner_id==""):
				del name_to_id[name]
			else:
				print("Registering " + name + " with id: " + str(hash_string(name)))
				name_to_id[name] = owner_id


	chat = Chat(local_address, remote_address, name)
	print("our address is: %s:%s" % (local_address.ip, local_address.port))
	print("our id is: " + str(hash(local_address)))
	print("our name is: " + name + " name id: " + str(hash_string(name)))
	if remote_address is not None:
		remote_hash = str(hash(remote_address))
		print("bootstrap address is: %s:%s id: %s" % \
			  (remote_address.ip, remote_address.port, remote_hash))
	print_usage()

	chat.set_msg_handler(msg_printer)
	chat.set_join_handler(join_registered)
	chat.set_leave_handler(leave_registered)
	chat.set_send_handler(send_registered)
	chat.set_transfer_handler(transfer_registered)
	chat.set_set_name_handler(set_name_registered)
	chat.set_get_name_handler(get_name_registered)
	chat.set_name_return_handler(name_return_registered)
	chat.set_name_handler(name_registered)
	chat.set_name()

	end = False
	while not end:
		user_input = input().split(" ", 2)
		if len(user_input) == 1 and user_input[0] == "exit":
			end = True
			chat.shutdown()
		elif len(user_input) == 3 and user_input[0] == "msg":
			chat.send_msg(user_input[1], user_input[2])
		elif len(user_input) == 2 and user_input[0] == "join":
			chat.join_request(str(hash(local_address)), user_input[1])
		elif len(user_input) == 2 and user_input[0] == "leave":
			chat.leave_request(str(hash(local_address)), user_input[1])
		elif len(user_input) == 3 and user_input[0] == "send":
			chat.send_request(user_input[2], user_input[1])
		else:
			print("unknown command")


if __name__ == "__main__":
	import sys

	main_program()
