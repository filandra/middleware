import java.rmi.Remote; 
import java.rmi.RemoteException; 

public interface NodeFactory extends Remote {	
	public NodeRemote getRemoteNode() throws RemoteException;	
}