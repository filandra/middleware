import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.rmi.server.UnicastRemoteObject;

public class NodeRemoteImpl extends UnicastRemoteObject implements NodeRemote {
	private final Set<NodeRemote> nodes = new HashSet<NodeRemote>();
        
        public NodeRemoteImpl () throws RemoteException
	{
		super (); 
	}

	@Override
	public Set<NodeRemote> getNeighbors() throws RemoteException {
		return nodes;
	}

	@Override
	public Map<NodeRemote, Integer> getTransitiveNeighbors(int distance) throws RemoteException {
		if (distance <= 0)
			throw new IllegalArgumentException("Argument distance must be positive");

		Map<NodeRemote, Integer> nodeToDistance = new HashMap<NodeRemote, Integer>();
		Set<NodeRemote> currentLayer = new HashSet<NodeRemote>();

		// Initial node at zero-distance
		currentLayer.add(this);

		// Closure  for each level of i-distant nodes
		for (int i = 0; i < distance; ++i) {
			Set<NodeRemote> nextLayer = new HashSet<NodeRemote>();

			// Use nodes which are in the current level 
			for (NodeRemote node : currentLayer) {
				if (!nodeToDistance.containsKey(node)) {
					nodeToDistance.put(node, i);
					nextLayer.addAll(node.getNeighbors());
				}
			}

			// Move to the next layer
			currentLayer = nextLayer;
		}

		// Handle the last layer
		for (NodeRemote node : currentLayer) {
			if (!nodeToDistance.containsKey(node))
				nodeToDistance.put(node, distance);
		}

		return nodeToDistance;
	}

	@Override
	public void addNeighbor(NodeRemote neighbor) {
		nodes.add(neighbor);
	}
}
