import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class NodeFactoryImpl extends UnicastRemoteObject implements NodeFactory {    
        int counter = 0;
        public NodeFactoryImpl () throws RemoteException
	{
		super (); 
	}

	@Override
	public NodeRemote getRemoteNode() throws RemoteException {
            NodeRemoteImpl node = new NodeRemoteImpl();                 
            return node;            
	}
}
