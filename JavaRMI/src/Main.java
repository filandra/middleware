
import java.util.Random;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class Main {
    // How many nodes and how many edges to create.

    private static final int GRAPH_NODES_CONNECT_SOME = 300;
    private static final int GRAPH_EDGES_CONNECT_SOME = 600;
    private static final int GRAPH_NODES_CONNECT_ALL = 300;        

    // How many searches to perform
    private static final int SEARCHES = 50;

    private static Node[] nodes;
    private static NodeRemote[] remoteNodes;

    private static Random random = new Random();
    private static Searcher searcher = null;
    private static RemoteSearcher serverSearcher = null;
    private static NodeFactory nodeFactory = null;
    
    private static int transitiveNum = 4;
    private static String adress = "";

    private static void init() {
        try {
            searcher = new SearcherImpl();
            // Use the registry on this host to find the server.
            // The host name must be changed if the server uses
            // another computer than the client!
            serverSearcher = (RemoteSearcher) Naming.lookup("//" + adress + "/serverSearcher");
            nodeFactory = (NodeFactory) Naming.lookup("//" + adress + "/nodeFactory");
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    /**
     * Creates nodes of a graph.
     *
     * @param howMany number of nodes
     */
    public static void createNodes(int howMany) throws RemoteException {
        nodes = new Node[howMany];
        remoteNodes = new NodeRemote[howMany];

        for (int i = 0; i < howMany; i++) {
            nodes[i] = new NodeImpl();
            remoteNodes[i] = nodeFactory.getRemoteNode();
        }
    }

    /**
     * Creates a fully connected graph.
     */
    public static void connectAllNodes() throws RemoteException {
        for (int idxFrom = 0; idxFrom < nodes.length; idxFrom++) {
            for (int idxTo = idxFrom + 1; idxTo < nodes.length; idxTo++) {
                nodes[idxFrom].addNeighbor(nodes[idxTo]);
                nodes[idxTo].addNeighbor(nodes[idxFrom]);
                remoteNodes[idxFrom].addNeighbor(remoteNodes[idxTo]);
                remoteNodes[idxTo].addNeighbor(remoteNodes[idxFrom]);
            }
        }
    }

    /**
     * Creates a randomly connected graph.
     *
     * @param howMany number of edges
     */
    public static void connectSomeNodes(int howMany) throws RemoteException {
        for (int i = 0; i < howMany; i++) {
            final int idxFrom = random.nextInt(nodes.length);
            final int idxTo = random.nextInt(nodes.length);

            nodes[idxFrom].addNeighbor(nodes[idxTo]);
            remoteNodes[idxFrom].addNeighbor(remoteNodes[idxTo]);
        }
    }

    /**
     * Runs a quick measurement on the graph.
     *
     * @param howMany number of measurements
     */
    public static void searchBenchmark(int howMany) throws RemoteException {
        // Display measurement header.
        System.out.printf("%7s %8s %13s %13s %13s %13s %13s %13s %13s %13s\n", "Attempt", "Distance", "Task1-Time", "Task1-TTime", "Task2-Time", "Task2-TTime", "Task3-Time", "Task3-TTime", "Task4-Time", "Task4-TTime");
        long durationAvg = 0;
        long TdurationAvg = 0;
        long remoteSearchDurationAvg = 0;
        long remoteSearchTDurationAvg = 0;
        long remoteNodeDurationAvg = 0;
        long remoteNodeTDurationAvg = 0;
        long remoteNodeSearchDurationAvg = 0;
        long remoteNodeSearchTDurationAvg = 0;

        for (int i = 0; i < howMany; i++) {
            // Select two random nodes.
            final int idxFrom = random.nextInt(nodes.length);
            final int idxTo = random.nextInt(nodes.length);

            // Calculate distance, measure operation time
            final long startTimeNs = System.nanoTime();
            final int distance = searcher.getDistance(nodes[idxFrom], nodes[idxTo]);
            final long durationNs = (System.nanoTime() - startTimeNs) / 1000;                        

            // Calculate transitive distance, measure operation time
            final long startTimeTransitiveNs = System.nanoTime();
            final int distanceTransitive = searcher.getDistanceTransitive(4, nodes[idxFrom], nodes[idxTo]);
            final long durationTransitiveNs = (System.nanoTime() - startTimeTransitiveNs) / 1000;           

            // Calculate distance, measure operation time, remote searcher
            final long remoteSearchStartTimeNs = System.nanoTime();
            final int remoteSearchDistance = serverSearcher.getDistance(nodes[idxFrom], nodes[idxTo]);
            final long remoteSearchDurationNs = (System.nanoTime() - remoteSearchStartTimeNs) / 1000;

            // Calculate transitive distance, measure operation time, remote searcher
            final long remoteSearchStartTimeTransitiveNs = System.nanoTime();
            final int remoteSearchDistanceTransitive = serverSearcher.getDistanceTransitive(4, nodes[idxFrom], nodes[idxTo]);
            final long remoteSearchDurationTransitiveNs = (System.nanoTime() - remoteSearchStartTimeTransitiveNs) / 1000;
            
            // Calculate distance, measure operation time, remote nodes, local searcher
            final long remoteNodeStartTimeNs = System.nanoTime();
            final int remoteNodeDistance = searcher.getDistance(remoteNodes[idxFrom], remoteNodes[idxTo]);
            final long remoteNodeDurationNs = (System.nanoTime() - remoteNodeStartTimeNs) / 1000;

            // Calculate transitive distance, measure operation time, remote nodes, local searcher
            final long remoteNodeStartTimeTransitiveNs = System.nanoTime();
            final int remoteNodeDistanceTransitive = searcher.getDistanceTransitive(4, remoteNodes[idxFrom], remoteNodes[idxTo]);
            final long remoteNodeDurationTransitiveNs = (System.nanoTime() - remoteNodeStartTimeTransitiveNs) / 1000;            
            
            // Calculate distance, measure operation time, remote nodes, remote searcher
            final long remoteNodeSearchStartTimeNs = System.nanoTime();
            final int remoteNodeSearchDistance = serverSearcher.getDistance(remoteNodes[idxFrom], remoteNodes[idxTo]);
            final long remoteNodeSearchDurationNs = (System.nanoTime() - remoteNodeSearchStartTimeNs) / 1000;

            // Calculate transitive distance, measure operation time, remote nodes, remote searcher
            final long remoteNodeSearchStartTimeTransitiveNs = System.nanoTime();
            final int remoteNodeSearchDistanceTransitive = serverSearcher.getDistanceTransitive(4, remoteNodes[idxFrom], remoteNodes[idxTo]);
            final long remoteNodeSearchDurationTransitiveNs = (System.nanoTime() - remoteNodeSearchStartTimeTransitiveNs) / 1000;           

            if (distance != distanceTransitive || distance != remoteSearchDistance || distance != remoteSearchDistanceTransitive || distance != remoteNodeDistance || distance != remoteNodeDistanceTransitive || distance != remoteNodeSearchDistance || distance != remoteNodeSearchDistanceTransitive) {
                System.out.printf("Algorithms inconsistent\n");
            } else {
                // Print the measurement result.
                System.out.printf("%7d %8d %13d %13d %13d %13d %13d %13d %13d %13d\n", i, distance, durationNs, durationTransitiveNs, remoteSearchDurationNs, remoteSearchDurationTransitiveNs, remoteNodeDurationNs, remoteNodeDurationTransitiveNs, remoteNodeSearchDurationNs, remoteNodeSearchDurationTransitiveNs);
                durationAvg += durationNs;
                TdurationAvg += durationTransitiveNs;
                remoteSearchDurationAvg += remoteSearchDurationNs;
                remoteSearchTDurationAvg += remoteSearchDurationTransitiveNs;
                remoteNodeDurationAvg += remoteNodeDurationNs;
                remoteNodeTDurationAvg += remoteNodeDurationTransitiveNs;
                remoteNodeSearchDurationAvg += remoteNodeSearchDurationNs;
                remoteNodeSearchTDurationAvg += remoteNodeSearchDurationTransitiveNs;
            }
        }
        System.out.printf("Average: %21d %13d %13d %13d %13d %13d %13d %13d\n", durationAvg / howMany, TdurationAvg / howMany, remoteSearchDurationAvg / howMany, remoteSearchTDurationAvg / howMany, remoteNodeDurationAvg / howMany, remoteNodeTDurationAvg / howMany, remoteNodeSearchDurationAvg / howMany, remoteNodeSearchTDurationAvg / howMany);
    }

    public static void main(String[] args) throws RemoteException {
        
        System.out.println();
        
        if(args[0].isEmpty()){
            System.out.println("Parameter must be the adress of computer with server and registry.");
            System.out.println("For example localhost or u-pl17.ms.mff.cuni.cz.");
            System.out.println("Don't forget to update the run-server file with proper adress!");
            System.exit(0);
        }        
        adress = args[0];
        
        try{
            if(!args[1].isEmpty()){
                transitiveNum = Integer.parseInt(args[1]);
            }
        }catch(Exception e){
            
        }
        
        System.out.printf("Task 6. getTransitiveNeighbors parameter = %d\n", transitiveNum);
        
        init();
        
        System.out.printf("Connect some nodes: %d nodes %d connections \n", GRAPH_NODES_CONNECT_SOME, GRAPH_EDGES_CONNECT_SOME);
        createNodes(GRAPH_NODES_CONNECT_SOME);
        //connectAllNodes();
        connectSomeNodes(GRAPH_EDGES_CONNECT_SOME);
        try {
            searchBenchmark(SEARCHES);
        } catch (Exception e) {
            System.out.println("Connect some - searchBenchmark error");
            System.out.println(e.toString());
        }
        
        System.out.println();
        
        System.out.printf("Connect all nodes: %d nodes \n", GRAPH_NODES_CONNECT_ALL);        
        createNodes(GRAPH_NODES_CONNECT_ALL);
        //connectAllNodes();
        connectAllNodes();
        try {
            searchBenchmark(SEARCHES);
        } catch (Exception e) {
            System.out.println("Connect all - searchBenchmark error");
            System.out.println(e.toString());
        }   
    }
}
