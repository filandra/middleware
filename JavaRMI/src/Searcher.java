import java.rmi.Remote; 
import java.rmi.RemoteException; 

public interface Searcher {
	public static final int DISTANCE_INFINITE = -1;
	public int getDistance(NodeRemote from, NodeRemote to) throws RemoteException;
	public int getDistanceTransitive(int neighborDistance, NodeRemote from, NodeRemote to) throws RemoteException;
        public int getDistance(Node from, Node to);
	public int getDistanceTransitive(int neighborDistance, Node from, Node to);
}
