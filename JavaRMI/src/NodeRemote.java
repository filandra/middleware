import java.util.Map;
import java.util.Set;
import java.io.Serializable;
import java.rmi.Remote; 
import java.rmi.RemoteException; 

public interface NodeRemote extends Remote {
	Set<NodeRemote> getNeighbors() throws RemoteException;
	Map<NodeRemote, Integer> getTransitiveNeighbors(int distance) throws RemoteException;
	void addNeighbor(NodeRemote neighbor) throws RemoteException;
}
