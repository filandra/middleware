
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Client {

    /**
     * **	CONSTANTS	***
     */
    // name of the property specifying client's name
    public static final String CLIENT_NAME_PROPERTY = "clientName";

    // name of the topic for publishing offers
    public static final String OFFER_TOPIC = "Offers";

    public static final String GOODS_NAME = "goodsName";

    public static final String ACCOUNT_NUMBER = "AccountNumber";

    public static final String CONFIRM = "confirm";

    public static final String TEXT = "text";

    public static final String CANCEL = "cancel";

    public static final String RETURN = "return";

    /**
     * **	PRIVATE VARIABLES	***
     */
    Object goodsLock = new Object();

    // client's unique name
    private String clientName;

    // client's account number
    private int accountNumber;

    // offered goods, mapped by name
    private Map<String, Goods> offeredGoods;

    // available goods, mapped by seller's name 
    private Map<String, List<Goods>> availableGoods;

    // reserved goods, mapped by name of the goods
    private Map<String, Goods> reservedGoods;

    // buyer's names, mapped by their account numbers
    private Map<Integer, String> reserverAccounts;

    // buyer's reply destinations, mapped by their names
    private Map<String, Destination> reserverDestinations;

    // connection to the broker
    private Connection conn;

    // session for user-initiated synchronous messages
    private Session clientSession;

    // session for listening and reacting to asynchronous messages
    private Session eventSession;

    // sender for the clientSession
    private MessageProducer clientSender;

    // sender for the eventSession
    private MessageProducer eventSender;

    // receiver of synchronous replies
    private MessageConsumer replyReceiver;

    // topic to send and receiver offers
    private Topic offerTopic;

    // queue for sending messages to bank
    private Queue toBankQueue;

    // queue for receiving synchronous replies
    private Queue replyQueue;

    // random number generator
    private Random rnd;

    // reader of lines from stdin
    private LineNumberReader in = new LineNumberReader(new InputStreamReader(System.in));

    /**
     * **	PRIVATE METHODS	***
     */
    /*
	 * Constructor, stores clientName, connection and initializes maps
     */
    private Client(String clientName, Connection conn) {
        this.clientName = clientName;
        this.conn = conn;

        // initialize maps
        offeredGoods = new HashMap<String, Goods>();
        availableGoods = new HashMap<String, List<Goods>>();

        reservedGoods = new HashMap<String, Goods>();
        reserverAccounts = new HashMap<Integer, String>();
        reserverDestinations = new HashMap<String, Destination>();

        // generate some goods
        rnd = new Random();
        for (int i = 0; i < 10; i++) {
            addGoods();
        }
    }

    /*
	 * Generate a goods item
     */
    private void addGoods() {
        String name = "";

        for (int i = 0; i < 4; i++) {
            char c = (char) ('A' + rnd.nextInt('Z' - 'A'));
            name += c;
        }

        offeredGoods.put(name, new Goods(name, rnd.nextInt(10000)));
    }

    /*
	 * Set up all JMS entities, get bank account, publish first goods offer 
     */
    private void connect() throws JMSException {
        // create two sessions - one for synchronous and one for asynchronous processing
        clientSession = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
        eventSession = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // create (unbound) senders for the sessions
        clientSender = clientSession.createProducer(null);
        eventSender = eventSession.createProducer(null);

        // create queue for sending messages to bank
        toBankQueue = clientSession.createQueue(Bank.BANK_QUEUE);
        // create a temporary queue for receiving messages from bank
        Queue fromBankQueue = eventSession.createTemporaryQueue();

        // temporary receiver for the first reply from bank
        // note that although the receiver is created within a different session
        // than the queue, it is OK since the queue is used only within the
        // client session for the moment
        MessageConsumer tmpBankReceiver = clientSession.createConsumer(fromBankQueue);

        // start processing messages
        conn.start();

        // request a bank account number
        Message msg = eventSession.createTextMessage(Bank.NEW_ACCOUNT_MSG);
        msg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);
        // set ReplyTo that Bank will use to send me reply and later transfer reports
        msg.setJMSReplyTo(fromBankQueue);
        clientSender.send(toBankQueue, msg);

        // get reply from bank and store the account number
        TextMessage reply = (TextMessage) tmpBankReceiver.receive();
        accountNumber = Integer.parseInt(reply.getText());
        System.out.println("Account number: " + accountNumber);

        // close the temporary receiver
        tmpBankReceiver.close();

        // temporarily stop processing messages to finish initialization
        conn.stop();

        /* Processing bank reports */
        // create consumer of bank reports (from the fromBankQueue) on the event session
        MessageConsumer bankReceiver = eventSession.createConsumer(fromBankQueue);

        // set asynchronous listener for reports, using anonymous MessageListener
        // which just calls our designated method in its onMessage method
        bankReceiver.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message msg) {
                try {
                    processBankReport(msg);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        // TODO finish the initialization
        /* Step 1: Processing offers */
        // Warning - možná vybraná špatná session
        // create a topic both for publishing and receiving offers
        // hint: Sessions have a createTopic() method        
        offerTopic = eventSession.createTopic(Client.OFFER_TOPIC);

        // create a consumer of offers from the topic using the event session
        MessageConsumer offerReceiver = eventSession.createConsumer(offerTopic);
        // set asynchronous listener for offers (see above how it can be done)
        // which should call processOffer()
        offerReceiver.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message msg) {
                try {
                    processOffer(msg);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });
        /* Step 2: Processing sale requests */

        // create a queue for receiving sale requests (hint: Session has createQueue() method)
        // note that Session's createTemporaryQueue() is not usable here, the queue must have a name
        // that others will be able to determine from clientName (such as clientName + "SaleQueue")
        Queue receivedRequestQueue = eventSession.createQueue(clientName + "SaleQueue");

        // create consumer of sale requests on the event session
        MessageConsumer requestReceiver = eventSession.createConsumer(receivedRequestQueue);

        // set asynchronous listener for sale requests (see above how it can be done)
        // which should call processSale()
        requestReceiver.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message msg) {
                try {
                    processSale(msg);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        // end TODO
        // create temporary queue for synchronous replies
        replyQueue = clientSession.createTemporaryQueue();

        // create synchronous receiver of the replies
        replyReceiver = clientSession.createConsumer(replyQueue);

        // restart message processing
        conn.start();

        // send list of offered goods
        Message offersMsg = clientSession.createTextMessage(Bank.OFFERS_QUERY);
        clientSender.send(toBankQueue, offersMsg);
    }

    /*
	 * Publish a list of offered goods
	 * Parameter is an (unbound) sender that fits into current session
	 * Sometimes we publish the list on user's request, sometimes we react to an event
     */
    private void publishGoodsList(MessageProducer sender) throws JMSException {
        // TODO

        // create a message (of appropriate type) holding the list of offered goods
        // which can be created like this: new ArrayList<Goods>(offeredGoods.values())
        synchronized (goodsLock) {
            Message msg = clientSession.createObjectMessage(new ArrayList<Goods>(offeredGoods.values()));
            msg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);

            // don't forget to include the clientName in the message so other clients know
            // who is sending the offer - see how connect() does it when sending message to bank
            // send the message using the sender passed as parameter                 
            sender.send(offerTopic, msg);
        }

    }

    /*
	 * Send empty offer and disconnect from the broker 
     */
    private void disconnect() throws JMSException {
        // delete all offered goods
        synchronized (goodsLock) {
            offeredGoods.clear();

            // send the empty list to indicate client quit
            publishGoodsList(clientSender);

            // close the connection to broker
            conn.close();
        }
    }

    /*
	 * Print known goods that are offered by other clients
     */
    private void list() {
        System.out.println("Available goods (name: price):");
        // iterate over sellers
        synchronized (availableGoods) {
            for (String sellerName : availableGoods.keySet()) {
                System.out.println("From " + sellerName);
                // iterate over goods offered by a seller
                for (Goods g : availableGoods.get(sellerName)) {
                    System.out.println("  " + g);
                }
            }
        }
    }

    /*
	 * Main interactive user loop
     */
    private void loop() throws IOException, JMSException {
        // first connect to broker and setup everything
        connect();

        loop:
        while (true) {
            System.out.println("\nAvailable commands (type and press enter):");
            System.out.println(" l - list available goods");
            System.out.println(" p - publish list of offered goods");
            System.out.println(" b - buy goods");
            System.out.println(" m - query balance");
            System.out.println(" q - quit");
            // read first character
            int c = in.read();
            // throw away rest of the buffered line
            while (in.ready()) {
                in.read();
            }
            switch (c) {
                case 'q':
                    disconnect();
                    break loop;
                case 'b':
                    buy();
                    break;
                case 'l':
                    list();
                    break;
                case 'm':
                    System.out.println("Your balance is " + getBalance(clientSender));
                    break;
                case 'p':
                    publishGoodsList(clientSender);
                    System.out.println("List of offers published");
                    break;
                case '\n':
                default:
                    break;
            }
        }
    }

    /*
	 * Perform buying of goods
     */
    private void buy() throws IOException, JMSException {
        // get information from the user
        System.out.println("Enter seller name:");
        String sellerName = in.readLine();
        System.out.println("Enter goods name:");
        String goodsName = in.readLine();

        // check if the seller exists
        synchronized (availableGoods) {
            List<Goods> sellerGoods = availableGoods.get(sellerName);
            if (sellerGoods == null) {
                System.out.println("Seller does not exist: " + sellerName);
                return;
            }
        }

        // TODO
        // First consider what message types clients will use for communicating a sale
        // we will need to transfer multiple values (of String and int) in each message 
        // MapMessage? ObjectMessage? TextMessage with extra properties?
        /* Step 1: send a message to the seller requesting the goods */
        // create local reference to the seller's queue
        // similar to Step 2 in connect() but using sellerName instead of clientName
        Queue sellerQeue = eventSession.createQueue(sellerName + "SaleQueue");

        // create message requesting sale of the goods
        // includes: clientName, goodsName, accountNumber
        // also include reply destination that the other client will use to send reply (replyQueue)
        // how? see how connect() uses SetJMSReplyTo()
        MapMessage msg = clientSession.createMapMessage();
        msg.setString(TEXT, "");
        msg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);
        msg.setString(GOODS_NAME, goodsName);
        msg.setInt(ACCOUNT_NUMBER, accountNumber);
        // set ReplyTo that Bank will use to send me reply and later transfer reports
        msg.setJMSReplyTo(replyQueue);

        // send the message (with clientSender)
        clientSender.send(sellerQeue, msg);

        /* Step 2: get seller's response and process it */
        // receive the reply (synchronously, using replyReceiver)
        TextMessage replySeller = (TextMessage) replyReceiver.receive();

        // parse the reply (depends on your selected message format)
        // distinguish between "sell denied" and "sell accepted" message
        // in case of "denied", report to user and return from this method
        int price = 0;
        int sellerAccount = Integer.parseInt(replySeller.getText());

        // Záporné číslo účtu na označení zamítnutí
        if (sellerAccount < 0) {
            System.out.println("Sell denied.");
            return;
        } else {
            // in case of "accepted"
            // - obtain seller's account number and price to pay                                    
            try {
                System.out.println("Enter ammount of money to send:");
                price = Integer.parseInt(in.readLine());
            } catch (Exception e) {
                System.out.println("Error, setting correct price");
                price = replySeller.getIntProperty(goodsName);
            }
        }

        /* Step 3: send message to bank requesting money transfer */
        // create message ordering the bank to send money to seller
        MapMessage bankMsg = clientSession.createMapMessage();
        bankMsg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);
        bankMsg.setInt(Bank.ORDER_TYPE_KEY, Bank.ORDER_TYPE_SEND);
        bankMsg.setInt(Bank.ORDER_RECEIVER_ACC_KEY, sellerAccount);
        bankMsg.setInt(Bank.AMOUNT_KEY, price);
        bankMsg.setJMSReplyTo(replyQueue);

        System.out.println("Sending $" + price + " to account " + sellerAccount);

        // send message to bank
        clientSender.send(toBankQueue, bankMsg);

        /* Step 4: wait for seller's sale confirmation */
        // receive the confirmation, similar to Step 2
        MapMessage replyBankOrSeller = (MapMessage) replyReceiver.receive();

        // parse message and verify it's confirmation message                                		
        // report successful sale to the user
        if (replyBankOrSeller.getString(TEXT).equals(CONFIRM)) {
            System.out.println("Bought " + replyBankOrSeller.getString(GOODS_NAME));
            synchronized (goodsLock) {
                offeredGoods.put(goodsName, new Goods(goodsName, price));
            }
            publishGoodsList(clientSender);
        } else {
            if (replyBankOrSeller.getString(TEXT).equals(Bank.LOW_BALANCE)) {
                System.out.println("Transaction canceled - low balance.");

                // Informuj prodávajícího, že se obchod ruší
                MapMessage msgCancel = clientSession.createMapMessage();
                msg.setString(TEXT, CANCEL);
                msg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);
                msg.setString(GOODS_NAME, goodsName);
                msg.setInt(ACCOUNT_NUMBER, accountNumber);
                clientSender.send(sellerQeue, msg);
            } else {
                if (replyBankOrSeller.getString(TEXT).equals(RETURN)) {
                    System.out.println("Transaction canceled - not enough money sent.");
                }
            }
        }
    }

    /*
	 * Process a message with goods offer
     */
    private void processOffer(Message msg) throws JMSException {
        // TODO

        // parse the message, obtaining sender's name and list of offered goods   
        ObjectMessage objectMessage = (ObjectMessage) msg;
        @SuppressWarnings("unchecked")
        List<Goods> lst = (List<Goods>) objectMessage.getObject();
        String sender = msg.getStringProperty(Client.CLIENT_NAME_PROPERTY);

        // should ignore messages sent from myself
        // if (clientName.equals(sender)) ...          
        synchronized (availableGoods) {
            if (!clientName.equals(sender)) {
                if (lst.isEmpty()) {
                    availableGoods.remove(sender);
                } else {
                    availableGoods.put(sender, lst);
                }
            }
        }
        // store the list into availableGoods (replacing any previous offer)
        // empty list means disconnecting client, remove it from availableGoods completely
//		if (lst.isEmpty()) {
//			availableGoods.remove(sender);
//		} else {
//			availableGoods.put(sender, lst);
//		}
    }

    /*
	 * Process message requesting a sale
     */
    private void processSale(Message msg) throws JMSException {
        // TODO

        /* Step 1: parse the message */
        // distinguish that it's the sale request message
        // obtain buyer's name (buyerName), goods name (goodsName) , buyer's account number (buyerAccount)
        MapMessage mapMsg = (MapMessage) msg;

        String buyerName = mapMsg.getStringProperty(CLIENT_NAME_PROPERTY);
        String goodsName = mapMsg.getString(GOODS_NAME);
        int buyerAccount = mapMsg.getInt(ACCOUNT_NUMBER);

        // also obtain reply destination (buyerDest)
        // how? see for example Bank.processTextMessage()
        Destination buyerDest = mapMsg.getJMSReplyTo();
        /* Step 2: decide what to do and modify data structures accordingly */

        // check if we still offer this goods
//		Goods goods = offeredGoods.get(goodsName);
//		if (goods != null) ...
        synchronized (goodsLock) {
            Goods goods = offeredGoods.get(goodsName);

            if (!mapMsg.getString(TEXT).equals(CANCEL)) {
                if (goods != null) {
                    System.out.println("Reserving " + goodsName);
                    // if yes, we should remove it from offeredGoods and publish new list                
                    offeredGoods.remove(goodsName);
                    publishGoodsList(eventSender);
                    // also it's useful to create a list of "reserved goods" together with buyer's information
                    // such as name, account number, reply destination
                    // offeredGoods.remove(goodsName);
                    reservedGoods.put(buyerName, goods);
                    reserverAccounts.put(buyerAccount, buyerName);
                    reserverDestinations.put(buyerName, buyerDest);

                    /* Step 3: send reply message */
                    // prepare reply message (accept or deny)
                    // accept message includes: my account number (accountNumber), price (goods.price)
                    TextMessage textMsg = eventSession.createTextMessage(Integer.toString(accountNumber));
                    textMsg.setIntProperty(goodsName, goods.price);

                    // send reply
                    eventSender.send(buyerDest, textMsg);
                } else {
                    System.out.println(goodsName + " not in stock");

                    TextMessage textMsg = eventSession.createTextMessage("-1");

                    // send reply
                    eventSender.send(buyerDest, textMsg);
                }
            } else {
                System.out.println("Order for " + goodsName + " canceled.");
                // Odebrat objednávku z rezervovaných, pokud byla přerušena
                goods = reservedGoods.get(buyerName);
                offeredGoods.put(goodsName, goods);
                publishGoodsList(eventSender);

                reservedGoods.remove(buyerName, goods);
                reserverAccounts.remove(buyerAccount, buyerName);
                reserverDestinations.remove(buyerName, buyerDest);
            }
        }
    }

    /*
	 * Process message with (transfer) report from the bank
     */
    private void processBankReport(Message msg) throws JMSException {
        /* Step 1: parse the message */

        // Bank reports are sent as MapMessage
        if (msg instanceof MapMessage) {
            MapMessage mapMsg = (MapMessage) msg;
            // get report number
            int cmd = mapMsg.getInt(Bank.REPORT_TYPE_KEY);
            if (cmd == Bank.REPORT_TYPE_RECEIVED) {
                // get account number of sender and the amount of money sent
                int buyerAccount = mapMsg.getInt(Bank.REPORT_SENDER_ACC_KEY);
                int amount = mapMsg.getInt(Bank.AMOUNT_KEY);

                // match the sender account with sender
                String buyerName = reserverAccounts.get(buyerAccount);

                // match the reserved goods
                synchronized (goodsLock) {

                    Goods g = reservedGoods.get(buyerName);

                    System.out.println("Received $" + amount + " from " + buyerAccount);

                    /* Step 2: decide what to do and modify data structures accordingly */
                    // did he pay enough?
                    if (amount >= g.price) {
                        // get the buyer's destination
                        Destination buyerDest = reserverDestinations.get(buyerName);

                        // remove the reserved goods and buyer-related information
                        reserverDestinations.remove(buyerName);
                        reserverAccounts.remove(buyerAccount);
                        reservedGoods.remove(buyerName);

                        /* TODO Step 3: send confirmation message */
                        // prepare sale confirmation message
                        // includes: goods name (g.name)
                        MapMessage replyMsg = eventSession.createMapMessage();
                        replyMsg.setString(GOODS_NAME, g.name);
                        replyMsg.setString(TEXT, CONFIRM);

                        System.out.println("Sold " + g.name);

                        // send reply (destination is buyerDest)                                        
                        eventSender.send(buyerDest, replyMsg);

                    } else {
                        // Odebrat objednávku z rezervovaných, pokud nedorazilo dost peněz
                        Destination buyerDest = reserverDestinations.get(buyerName);

                        offeredGoods.put(g.name, g);
                        publishGoodsList(eventSender);

                        reservedGoods.remove(buyerName);
                        reserverAccounts.remove(buyerAccount, buyerName);
                        reserverDestinations.remove(buyerName);

                        MapMessage replyMsg = eventSession.createMapMessage();
                        replyMsg.setString(GOODS_NAME, g.name);
                        replyMsg.setString(TEXT, RETURN);

                        // send reply (destination is buyerDest)                                        
                        eventSender.send(buyerDest, replyMsg);

                        // Návrat peněz kupujícímu
                        MapMessage bankMsg = clientSession.createMapMessage();
                        bankMsg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);
                        bankMsg.setInt(Bank.ORDER_TYPE_KEY, Bank.ORDER_TYPE_RETURN);
                        bankMsg.setInt(Bank.ORDER_RECEIVER_ACC_KEY, buyerAccount);
                        bankMsg.setInt(Bank.AMOUNT_KEY, amount);
                        bankMsg.setJMSReplyTo(replyQueue);

                        System.out.println("Not enough money - returning $" + amount + " to account " + buyerAccount);
                        System.out.println("Order for " + g.name + " canceled.");

                        eventSender.send(toBankQueue, bankMsg);

                    }
                }
            } else {
                if (cmd == Bank.REPORT_TYPE_RETURN) {
                    int sellerAccount = mapMsg.getInt(Bank.REPORT_SENDER_ACC_KEY);
                    int amount = mapMsg.getInt(Bank.AMOUNT_KEY);

                    System.out.println("Returned $" + amount + " from account " + sellerAccount);

                } else {
                    if (cmd == Bank.REPORT_TYPE_OFFERS) {
                        publishGoodsList(eventSender);
                    } else {
                        System.out.println("Received unknown MapMessage:\n: " + msg);
                    }
                }
            }
        } else {
            System.out.println("Received unknown message:\n: " + msg);
        }

    }

    /**
     * ** PUBLIC METHODS ***
     */
    /*
	 * Main method, creates client instance and runs its loop
     */
    private int getBalance(MessageProducer sender) throws JMSException {
        // request a bank account number
        Message msg = clientSession.createTextMessage(Bank.BALANCE_QUERY);
        msg.setStringProperty(CLIENT_NAME_PROPERTY, clientName);
        // set ReplyTo that Bank will use to send me reply and later transfer reports
        msg.setJMSReplyTo(replyQueue);
        sender.send(toBankQueue, msg);

        TextMessage reply = (TextMessage) replyReceiver.receive();

        return Integer.parseInt(reply.getText());
    }

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Usage: ./client <clientName>");
            return;
        }

        Connection connection = null;

        Client client = null;

        try {
            // create connection to the broker.
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
            connection = connectionFactory.createConnection();

            // create instance of the client
            client = new Client(args[0], connection);

            // perform client loop
            client.loop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // always close the connection
                connection.close();
            } catch (Throwable ignore) {
                // ignore errors during close
            }
        }
    }
}
