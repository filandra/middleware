#include <iostream>

#include <omniORB4/CORBA.h>
#include "master.h"
#include <unistd.h>

using namespace std;
using namespace master;
using namespace consts; 

int main(int argc, char **argv) {
	try {
		if (argc != 2) {
			cout << "Usage: client IOR" << endl;
			return 0;
		}

		CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);

		CORBA::Object_var connectBase = orb->string_to_object(argv[1]);
		server_i_var connector = server_i::_narrow(connectBase);		
				

		CORBA::String_var peer  = "44520437";
		CORBA::LongLong key = 641940;		

		cout << "Connecting - peer: " << peer << " - key: " << key << endl;

		instance_i_var talker = connector->connect(peer, key);					
		cout << "Setting instance_i ready attribute" << endl;
		talker->ready(true);

		cout << "Checking if instance_i is idle" << endl;
		while(talker->idle() != true){		
			sleep(1);
			cout << "Checking if instance_i is idle" << endl;
		}					

		count_t_var cnt;				
		cnt->long_long_value(key);		
		octet_sequence_t_var status;			
		
		cout << "Getting status - peer: " << peer << " - key: " << key << endl;		

		talker->get_status(peer, cnt, status.out());		
			
		if(cnt->_d() == 1){
			cout << "Trying request with long cnt value" << endl;
			request_t req_long;		
			req_long.index.long_value(cnt->long_value());
			req_long.data <<= CORBA::Any::from_octet(status[cnt->long_value()]);	

			talker->request(req_long);			
		}else{			
			cout << "Trying request with short cnt value" << endl;
			request_t req_short;
			req_short.index.short_value(cnt->short_value());
			req_short.data <<= CORBA::Any::from_octet(status[cnt->short_value()]);

			talker->request(req_short);
		}

		cout << "Success!" << endl;
		cout << "Disconnecting" << endl;
		talker->disconnect();

		orb->destroy();
	} catch (const master::server_i::connection_e &ex) {		
		cerr << ex.cause << endl;
		return 1;
	} catch (const CORBA::SystemException &ex) {
		cerr << ex._name() << endl;		
		return 1;
	}
}
