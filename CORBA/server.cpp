#include <iostream>
#include <exception>
#include <omniORB4/CORBA.h>
#include "master.h"
#include <ctime>
#include <unistd.h>
#include <string.h>

using namespace std;
using namespace master;
using namespace consts;

class InstanceImpl : public POA_master::instance_i {
public:

	InstanceImpl(char*& changedPeer, CORBA::LongLong& changedKey){		
		this->changedPeer = changedPeer;
		this->changedKey = changedKey;

	}

	/*void sleep_set_idl(){
		sleep(5);
		idle_val = true;
	}*/

	CORBA::Boolean idle(){
		cout << "Idle read" << endl;
		// Správně by tu mělo být vytvoření novýho threadu, kterej by volal member funkci,
		//která by po chvíli spaní nastavila private idle_val
		//Zkoušel jsem std::thread t(&InstanceImpl::sleep_set_idl, this);
		//a další podobný varianty ale jaksi to nemůžu rozchodit
		if(pooler != 5){
			pooler++;
		}else{
			idle_val = true;
		}
		return idle_val;
	};

	CORBA::Boolean ready(){
		cout << "Ready read" << endl;
		return ready_val;
	};

	void ready(CORBA::Boolean _v){
		cout << "Ready set" << endl;
		ready_val = _v;
	};

	void get_status(const char* s_key, master::count_t& cnt, master::octet_sequence_t_out status){
	cout << "Giving status" << endl;
	
	if(strcmp(s_key, changedPeer) != 0 || cnt.long_long_value() != changedKey){
			string res_string = "Wrong params";
			const char* res = res_string.c_str();
			master::instance_i::protocol_e ex(res);
			throw ex;
		}

	status = new master::octet_sequence_t;
	status->length(8);
	for (int i = 0; i < 8; i++){
		status[i] = rand()%255;
	}
	
	if(rand()%2 == 1){
		cnt.long_value(rand()%8);
		check <<= CORBA::Any::from_octet(status[cnt.long_value()]);
		index_long = cnt.long_value();
		is_short = false;
	}else{
		cnt.short_value(rand()%8);
		check <<= CORBA::Any::from_octet(status[cnt.short_value()]);
		index_short = cnt.short_value();
		is_short = true;
	}

	};

	CORBA::Boolean request(const master::request_t& req){
	
		CORBA::Octet req_val;
		CORBA::Octet check_val;

		req.data >>= CORBA::Any::to_octet(req_val);
		check >>= CORBA::Any::to_octet(check_val);

		if(req_val != check_val){	
			string res_string = "Wrong params";
			const char* res = res_string.c_str();
			master::instance_i::protocol_e ex(res);
			throw ex;	
		}

		if(is_short == true){
			if(req.index._d() == 0 && req.index.short_value() == index_short){
				return true;
			}
		}else{
			if(req.index._d() == 1 && req.index.long_value() == index_long){
				return true;
			}
		}


		string res_string = "Wrong params";
		const char* res = res_string.c_str();
		master::instance_i::protocol_e ex(res);
		throw ex;	

	};

	void disconnect(){
		cout << "Disconnecting" << endl;
		// Hádám, že by se tu měl volat destruktor, ale při jeho zavolání dostanu error.
		//this->~InstanceImpl();		
	};
private:
	CORBA::Long index_long;
	CORBA::Short index_short;
	bool is_short;
	CORBA::Any check;
	char* changedPeer;
	CORBA::LongLong changedKey;
	CORBA::Boolean ready_val = false;
	CORBA::Boolean idle_val = false;
	bool idle_checked = false;	
	int pooler = 0;
};

class SimpleServant : public POA_master::server_i {
public:
	CORBA::Short ping(CORBA::Short val) {
		cout << "Pinged with value: " << val << endl;
		return val;
	};

	instance_i_ptr connect(char*& peer, CORBA::LongLong& key){
		cout << "Connection with peer: " << peer << " and key: " << key << endl;		
		if(key != primitive_hash(peer)){
			string res_string = "Wrong key: " + to_string(key) + " -  use: " + to_string(primitive_hash(peer));
			const char* res = res_string.c_str();
			master::server_i::connection_e ex(res);
			throw ex;
		}
	
		CORBA::string_free(peer);
		//Do some manipulation with the peer parameter			
		peer = generate_random_peer();
		// Do some manipulation with the key parameter
		key = primitive_hash(peer);
		
		char* peerCopy = CORBA::string_dup(peer);
		InstanceImpl *instance_impl = new InstanceImpl(peerCopy, key);
		instance_i_ptr instance = instance_impl->_this();
		return instance;
	};
private:
	CORBA::LongLong primitive_hash(char*& peer){
		return (CORBA::LongLong (*peer))*12345;
		
	}
	char* generate_random_peer(){
		char* peer = CORBA::string_alloc(consts::MAX_LEN);
		for (int i = 0; i < consts::MAX_LEN; i++){
			peer[i] = (rand() % 26) + 'A';
		}
		return peer;
	}
};

int main(int argc, char **argv) {
	try {
		srand(time(0));

		CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);

		CORBA::Object_var rootPOABase = orb->resolve_initial_references("RootPOA");
		PortableServer::POA_var rootPOA = PortableServer::POA::_narrow(rootPOABase);
		PortableServer::POAManager_var rootPOAManager = rootPOA->the_POAManager();

		SimpleServant* simpleServant = new SimpleServant;
		server_i_var simple = simpleServant->_this();

		cout << orb->object_to_string(simple) << endl;

		rootPOAManager->activate();
		orb->run();
	} catch (const CORBA::SystemException &ex) {
		cerr << ex._name() << endl;
		return 1;
	} catch (const exception &ex) {
		cerr << ex.what() << endl;
		return 1;
	}
}
