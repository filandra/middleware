2. úkol - Middleware
Adam Filandr

Poznámka ohledně problému s Marshallingem:
Server čas od času zařve System exception MARSHAL(YES,MARSHAL_StringIsTooLong) while (un)marshalling. Toto se děje pouze při opakovaném pokusu o připojení klientem k serveru a to navíc je někdy. Např. při volání ping() se tak neděje nikdy a při volání connect() většinou až po 2-3 připojení.
